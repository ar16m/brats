import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_files, output_path):
    """
    Use this function to combine t1ce and flair predictions and save the prediction slices as rgb slices
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        print(img_file)
        img_flair3 = scipy.misc.imread(re.sub("flair3m2","flair3m2",img_file))
        img_t1ce3 = scipy.misc.imread(re.sub("flair3m2","t1cem",img_file))
        img_predm = scipy.misc.imread(re.sub("flair3m2","predm",img_file))
        img_dpredm = scipy.misc.imread(re.sub("flair3m2","dpredm",img_file))
        
        img_flce = numpy.zeros((240,240), dtype='int16')
        img_flce[img_flair3>0]=2
        img_flce[img_t1ce3==1]=1
        img_flce[img_t1ce3==3]=3
        img_predm=img_predm*64
        img_dpredm=img_dpredm*64
        img_flce=img_flce*64
        img_crgb = numpy.stack((img_predm,img_dpredm,img_flce), axis=2)
        img_slice=os.path.basename(re.sub("flair3m2","crgb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_crgb, cmin=0, cmax=255).save(outFilepath)
