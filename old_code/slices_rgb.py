import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_files, output_path):
    """
    Use this function to combine flair, t1ce, and t2 slices and pass path to flair as input_files
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        img_flair = scipy.misc.imread(re.sub("flair","flair",img_file))
        img_t1ce = scipy.misc.imread(re.sub("flair","t1ce",img_file))
        img_t2 = scipy.misc.imread(re.sub("flair","t2",img_file))
        img_rgb = numpy.stack((img_flair,img_t1ce,img_t2), axis=2)
        img_slice=os.path.basename(re.sub("flair","rgb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_rgb).save(outFilepath)
