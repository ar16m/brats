import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def process(dir_path,prefix,input_files, output_path):
    """
    Use this function to slice 3D images 0,1,2 orientations slice along different axes
    """

    resource.RLIMIT_NPROC=1
    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        print(patient)
        for orient in range(0,3):
            print(str(orient))
            slices= []
            for i in range(0,240):
                if orient==2 and (i < 42 or i > 196):
                    temp = numpy.zeros((240,240), dtype='int16')
                elif orient==2:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient+'_'+str(i-42)+'.png')
                else:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient +'_'+str(i)+'.png')
                slices.append(temp)
               

            volume = numpy.stack(slices, axis=orient)
            pred.append(volume)

         

        pred_0=pred[0].flatten()
        pred_1=pred[1].flatten()
        pred_2=pred[2].flatten()
        changes=numpy.where(pred_1==pred_2)
        
        for i in range(0,changes[0].size):
            pred_0[changes[0][i]]=pred_1[changes[0][i]]
        


        pred_comb=pred_0.reshape(240,240,240)
        predc_slices=[]
        for i in range(42,197):
            slice_pred = pred_comb[:,:,i]
            predc_slices.append(slice_pred)


        vol_pred = numpy.stack(predc_slices, axis=2)
        vol_pred[vol_pred==3]=4
        print(numpy.count_nonzero(vol_pred))
        print(vol_pred.shape)
        pred_nii=nibabel.Nifti1Image(vol_pred, numpy.eye(4))
        nii_name=os.path.basename(patient)+".nii.gz"
        outFilepath = output_path + "/" + nii_name
        nibabel.save(pred_nii,outFilepath)
        test=nibabel.load(outFilepath)
        print(test.get_data().shape)
        print(numpy.array_equal(test.get_data(),vol_pred))
