""" 
Code to create all input files for training first 18 deeplab classifiers
    Args:
        input_nii: Path to the top-level Brats data directory
            (with HGG and LGG as subdirectories for train set;
            validation, test has each case under this folder)
        output_path: Output location. Set up directory structure
                    output_path
                        |- orient_0
                            |-flair
                                |-t
                                |-nt
                                |-test
                                |-val
                                |-train
                            |-seg
                                ...
                            |-t1
                                ...
                            |-t2
                                ...
                            |-t1ce
                                ...
                            |-flair3
                                ...
                            |-t13
                                ...
                            |-t23
                                ...
                            |-t1ce3
                                ...
                            |-rgb
                                ...
                            |-rdb
                                ...
                        |-orient_1
                            ...
                        |-orient_2
                            ...
            data_split: train, test, val

"""

from preprocessing_utils import slicing3Dto2D 
from preprocessing_utils import create_image_from_3slices
from preprocessing_utils import create_3channel_rgb
from preprocessing_utils import create_3channel_t1ce_minus_t1
import sys

if __name__=="__main__":
    input_nii = sys.argv[1]
    output_path = sys.argv[2]
    slicing3Dto2D(input_nii, output_path, 0, data_split)
    slicing3Dto2D(input_nii, output_path, 1, data_split)
    slicing3Dto2D(input_nii, output_path, 2, data_split)
    create_image_from_3slices(output_path+'/orient_0/flair/'+data_split, '/path/to/files/list', output_path+'/orient_0/flair3', 'flair', 240)
    create_image_from_3slices(output_path+'/orient_0/t1ce/'+data_split, '/path/to/files/list', output_path+'/orient_0/t1ce3', 't1ce', 240)
    create_image_from_3slices(output_path+'/orient_0/t1/'+data_split, '/path/to/files/list', output_path+'/orient_0/t13', 't1', 240)
    create_image_from_3slices(output_path+'/orient_0/t2/'+data_split, '/path/to/files/list', output_path+'/orient_0/t23', 't2', 240)
    create_image_from_3slices(output_path+'/orient_1/flair/'+data_split, '/path/to/files/list', output_path+'/orient_1/flair3', 'flair', 240)
    create_image_from_3slices(output_path+'/orient_1/t1ce/'+data_split, '/path/to/files/list', output_path+'/orient_1/t1ce3', 't1ce', 240)
    create_image_from_3slices(output_path+'/orient_1/t1/'+data_split, '/path/to/files/list', output_path+'/orient_1/t13', 't1', 240)
    create_image_from_3slices(output_path+'/orient_1/t2/'+data_split, '/path/to/files/list', output_path+'/orient_1/t23', 't2', 240)
    create_image_from_3slices(output_path+'/orient_2/flair/'+data_split, '/path/to/files/list', output_path+'/orient_2/flair3', 'flair', 155)
    create_image_from_3slices(output_path+'/orient_2/t1ce/'+data_split, '/path/to/files/list', output_path+'/orient_2/t1ce3', 't1ce', 155)
    create_image_from_3slices(output_path+'/orient_2/t1/'+data_split, '/path/to/files/list', output_path+'/orient_2/t13', 't1', 155)
    create_image_from_3slices(output_path+'/orient_2/t2/'+data_split, '/path/to/files/list', output_path+'/orient_2/t23', 't2', 155)
    # Only use slices with tumor for training
    if data_split == 'train':
        data_split = 't'
    create_3channel_rgb(output_path+'/orient_0/flair/'+data_split, output_path+'/orient_0/rgb/'+data_split)
    create_3channel_rgb(output_path+'/orient_1/flair/'+data_split, output_path+'/orient_1/rgb/'+data_split)
    create_3channel_rgb(output_path+'/orient_2/flair/'+data_split, output_path+'/orient_2/rgb/'+data_split)
    create_3channel_t1ce_minus_t1(output_path+'/orient_0/flair/'+data_split, output_path+'/orient_0/rdb/'+data_split)
    create_3channel_t1ce_minus_t1(output_path+'/orient_1/flair/'+data_split, output_path+'/orient_1/rdb/'+data_split)
    create_3channel_t1ce_minus_t1(output_path+'/orient_2/flair/'+data_split, output_path+'/orient_2/rdb/'+data_split)

