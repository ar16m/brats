import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def compute_majority(dir_path, input_files, prefix, output_path, save_orient):
    """
    Function to compute majority and save as slices in save_orient orientation
    Args:
        dir_path: path to the directory containig slices
        input_files: Text file with list of patients
        prefix: pred for reg prediction
                dpred for rdb prediction
                flair3pred
                t1ce3pred
                t13pred
                t23pred
                The folders of the input is:
                dir_path
                    |-flair3pred0
                    |-flair3pred1
                    |-flair3pred2
                    |-pred0
                    |-pred1
                    |-pred2
                    ...
        output_path: Full path for storing created images
            Should be already created
        save_orient: 0,1, or 2 based on how we want to compbine later
    """
    resource.RLIMIT_NPROC=1
    # create a list of patient IDs
    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        print(patient)
        # recombine pred slices and create one volume for each orientation
        for orient in range(0,3):
            print(str(orient))
            slices= []
            for i in range(0,240):
                if orient==2 and (i < 42 or i > 196):
                    temp = numpy.zeros((240,240), dtype='int16')
                elif orient==2:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient+'_'+str(i-42)+'.png')
                else:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient +'_'+str(i)+'.png')
                slices.append(temp)
               

            volume = numpy.stack(slices, axis=orient)
            pred.append(volume)

         

        pred_0=pred[0].flatten()
        pred_1=pred[1].flatten()
        pred_2=pred[2].flatten()
        #if prefix=="dpred":
        #    changes=numpy.where(pred_2==pred_0)
        #    for i in range(0,changes[0].size):
        #        pred_0[changes[0][i]]=pred_1[changes[0][i]]
        #    pred_comb=pred_0.reshape(240,240,240)
        #else:

        # orientation 2 is the default selected unless other 2 give same pred
        changes=numpy.where(pred_1==pred_0)
        
            for i in range(0,changes[0].size):
                pred_2[changes[0][i]]=pred_1[changes[0][i]]
            pred_comb=pred_2.reshape(240,240,240)
        

        # slice in save_orient direction
        predc_slices=[]
        if save_orient==2:
            for i in range(42,197):
                slice_pred = pred_comb[:,:,i]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i-42)+'.png')
        elif save_orient==1:
            for i in range(0,240):
                slice_pred = pred_comb[:,i,:]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i)+'.png')
        elif save_orient==0:
            for i in range(0,240):
                slice_pred = pred_comb[i,:,:]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i)+'.png')


def pack_pred_as_grayscale(dir_path, input_files, output_path, orient, pack_list):
    """
    Function to pack the predictions for different image combinations in one orientation 
    (or after using majority voting for combining orientations with poor prediction)
     Args:
        dir_path: path to the directory containig slices
        input_files: Text file with list of patients
        output_path: Full path for storing created images
            Should be already created
        orient: 0,1, or 2
        pack_list: list of pred types to combine
                    pred is rgb prediction
                    dpred is rdb prediction

    """

    resource.RLIMIT_NPROC=1
    # Create list of patient IDs
    f = open(input_files)
    lines = f.readlines()
    
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        slices=[]
        print(patient)
        # read and put the pred of diff types in a list
        for i in range(0,len(pack_list)):
            slices.append([])
        for j in range(0, len(pack_list)):
            for i in range(0,240):
                if orient==2 and (i < 42 or i > 196):
                    temp = numpy.zeros((240,240), dtype='int16')
                elif orient==2:
                    temp = scipy.misc.imread(dir_path+ '/' + pack_list[j]+'/'+patient+'_'+str(i-42)+'.png')
                else:
                    temp = scipy.misc.imread(dir_path+ '/' + pack_list[j]+'/'+patient+'_'+str(i)+'.png')
                slices[j].append(temp)

        volumes=[]
        packed_vol=numpy.zeros((240,240,240), dtype='int16')
        for i in range(0,len(pack_list)):
            print(str(i))
            # stack the slices from one pred
            temp=numpy.stack(slices[i], axis=orient)
            volumes.append(temp)
            # actual computation for bit packing
            volumes[i]=volumes[i]*(2**(2*i))
            packed_vol=numpy.add(packed_vol,volumes[i])
       
        if len(pack_list) < 4:
            # multiply to use MSBs
            packed_vol=packed_vol*(4-len(pack_list))*4
            print(str(4-len(pack_list)))
        # slice axially and save
        for i in range(42,197):
            slice_pred = packed_vol[:,:,i]
            img_slice=patient+'_'+str(i-42)+'_pack'+str(orient)+'.png'
            outFilepath = output_path + "/" + img_slice
            scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(outFilepath)


def combine_packed_channels(input_path):
    """
    Use this function to combine the packed grayscale 
    prediction slices as packed rgb prediction slices
    Args:
        input_path: path to the directory containig pack0 slices
                    We pass path to pack0 folder shown below
            The structure should be:
            input_path/..
                |-pack0/
                |-pack1/
                |-pack2/
                |-prgb/ (for storing rgb packed preds)

    """
    
    # create list of files
    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        # read packed predictions for 3 channels 0,1,2
        img_pack0 = scipy.misc.imread(re.sub("pack0","pack0",img_file))
        img_pack1 = scipy.misc.imread(re.sub("pack0","pack1",img_file))
        img_pack2 = scipy.misc.imread(re.sub("pack0","pack2",img_file))
        # combine 3 packed slices to get rgb packed prediction
        img_prgb = numpy.stack((img_pack0,img_pack1,img_pack2), axis=2)
        img_slice=os.path.basename(re.sub("pack0","prgb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_prgb, cmin=0, cmax=255).save(outFilepath)
