import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def process(dir_path,prefix,input_files, output_path, save_orient):
    """
    Use this function to slice 3D images 0,1,2 orientations slice along different axes
    """

    resource.RLIMIT_NPROC=1
    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        print(patient)
        for orient in range(0,3):
            print(str(orient))
            slices= []
            for i in range(0,240):
                if orient==2 and (i < 42 or i > 196):
                    temp = numpy.zeros((240,240), dtype='int16')
                elif orient==2:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient+'_'+str(i-42)+'.png')
                else:
                    temp = scipy.misc.imread(dir_path+'/'+prefix + str(orient)+ '/' + patient +'_'+str(i)+'.png')
                slices.append(temp)
               

            volume = numpy.stack(slices, axis=orient)
            pred.append(volume)

         

        pred_0=pred[0].flatten()
        pred_1=pred[1].flatten()
        pred_2=pred[2].flatten()
        if prefix=="dpred":
            changes=numpy.where(pred_2==pred_0)
            for i in range(0,changes[0].size):
                pred_0[changes[0][i]]=pred_1[changes[0][i]]
            pred_comb=pred_0.reshape(240,240,240)
        else:
            changes=numpy.where(pred_1==pred_0)
        
            for i in range(0,changes[0].size):
                pred_2[changes[0][i]]=pred_1[changes[0][i]]
            pred_comb=pred_2.reshape(240,240,240)
        


        predc_slices=[]
        if save_orient==2:
            for i in range(42,197):
                slice_pred = pred_comb[:,:,i]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i-42)+'.png')
        elif save_orient==1:
            for i in range(0,240):
                slice_pred = pred_comb[:,i,:]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i)+'.png')
        elif save_orient==0:
            for i in range(0,240):
                slice_pred = pred_comb[i,:,:]
                scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(output_path + "/" + patient+'_'+str(i)+'.png')


