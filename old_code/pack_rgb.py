import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_files, output_path):
    """
    Use this function to save the packed prediction slices as rgb slices
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        img_pack0 = scipy.misc.imread(re.sub("pack0","pack0",img_file))
        img_pack1 = scipy.misc.imread(re.sub("pack0","pack1",img_file))
        img_pack2 = scipy.misc.imread(re.sub("pack0","pack2",img_file))
        img_prgb = numpy.stack((img_pack0,img_pack1,img_pack2), axis=2)
        img_slice=os.path.basename(re.sub("pack0","prgb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_prgb, cmin=0, cmax=255).save(outFilepath)
