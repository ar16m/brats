import nibabel
import scipy.misc
import numpy
import os
import re
import resource


def process(input_files, output_path):
    """
    Use this function to combine flair, t1ce-t1, and t2 slices and pass path to flair as input_files
    """
    resource.RLIMIT_NPROC=1
    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            print(fp)
            files.append(fp)


    for img_file in files:
        print(re.sub("flair","#",img_file))
        img_flair = scipy.misc.imread(re.sub("flair","flair",img_file))
        img_flair1 = numpy.zeros((240,240), dtype='int16')
        img_flair1[0 : img_flair.shape[0]+0, 0 : img_flair.shape[1]+0] = img_flair
        img_t1ce = scipy.misc.imread(re.sub("flair","t1ce",img_file))
        img_t1ce1 = numpy.zeros((240,240), dtype='int16')
        img_t1ce1[0 : img_t1ce.shape[0]+0, 0 : img_t1ce.shape[1]+0] = img_t1ce
        img_t2 = scipy.misc.imread(re.sub("flair","t2",img_file))
        img_t21 = numpy.zeros((240,240), dtype='int16')
        img_t21[0 : img_t2.shape[0]+0, 0 : img_t2.shape[1]+0] = img_t2
        img_t1 = scipy.misc.imread(re.sub("flair","t1",img_file))
        img_t11 = numpy.zeros((240,240), dtype='int16')
        img_t11[0 : img_t1.shape[0]+0, 0 : img_t1.shape[1]+0] = img_t1
        print(img_flair1.shape)
        print(img_t1ce1.shape)
        print(img_t21.shape)
        print(img_t11.shape)
        img_t1c = numpy.subtract(img_t11, img_t1ce1)
        img_slice=os.path.basename(re.sub("flair","t1c1",img_file))
        outFilepath = re.sub("rdb","t1c1",output_path) + "/" + img_slice
        scipy.misc.toimage(img_t1c).save(outFilepath)
        img_t1c1 = scipy.misc.imread(re.sub("flair","t1c1",img_file))
        img_rdb = numpy.stack((img_flair1,img_t21,img_t1c1), axis=2)
        img_slice=os.path.basename(re.sub("flair","rdb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_rdb).save(outFilepath)
