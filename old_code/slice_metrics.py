import nibabel
import scipy.misc
import numpy
import os
import re

def process(input_files,prefix):
    """
    Use this function to save the packed prediction slices as rgb slices
    """
    out=open("metrics","w")
    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 
    wt=0
    print(len(files))
    for img_file in files:
        img_seg = scipy.misc.imread(img_file)
        img_pred = scipy.misc.imread(re.sub("seg",prefix,re.sub("_seg","",img_file)))
        img_seg[img_seg>0]=1
        img_pred[img_pred>0]=1
        wt_slice=numpy.where(img_seg>0)[0].size
        wtp_slice=numpy.where(img_pred>0)[0].size
        img_pred[img_pred==0]=2
        wt_int=numpy.where(img_seg==img_pred)[0].size
        if (wt_slice+wtp_slice) > 0:
            dice=(2*wt_int)/(wt_slice+wtp_slice)
        else:
            dice=1
 
        out.write(img_file+":"+str(wt_slice)+":"+str(wtp_slice)+":"+str(wt_int)+":"+str(dice)+ "\n")
