import nibabel
import scipy.misc
import numpy
import os
import re



def process(dir_path, input_files, output_path, channel, num_slices):
    """
    Use this function to slice 3D images 0,1,2 orientations slice along different axes
    """


    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        print(patient)
#        print(re.sub("flair","#",img_file))
    #    read_name=re.sub("tumor","flair_all",img_file)
        for i in range(0,num_slices):
            img_prev = numpy.zeros((240,240), dtype='int16')
            img_next = numpy.zeros((240,240), dtype='int16')
            this_img = dir_path + '/' +patient+'_'+str(i)+ '_' +channel+'.png'
            print(this_img)
            prev_img = dir_path + '/' + patient+'_'+str(i-1)+'_'+channel+'.png'
            next_img = dir_path + '/' + patient+'_'+str(i+1)+'_'+channel+'.png'
            img_this = scipy.misc.imread(this_img)
            if i!=0:
                img_prev = scipy.misc.imread(prev_img)
            if i!=num_slices-1:
                img_next = scipy.misc.imread(next_img)
            img_ch3 = numpy.stack((img_prev,img_this,img_next), axis=2)
            img_slice=patient+'_'+str(i)+"_" +channel+"3.png"
            print(img_slice)
            outFilepath = output_path + "/" + img_slice
            scipy.misc.toimage(img_ch3).save(outFilepath)
