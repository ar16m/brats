"""
Functions for preprocessing before we train
the first set of 18 Deeplabv3+ classifiers
"""
import nibabel
import scipy.misc
import numpy
import os
import re

def slicing3Dto2D(input_nii, output_path, orientation, data_split):
    """
    Use this function to slice 3D images 0,1,2 orientations slice
    along different axes
    Args:
        input_nii: Path to the top-level Brats data directory 
            (with HGG and LGG as subdirectories for train set;
            validation, test has each case under this folder)
        output_path: Output location. Set up directory structure
                    output_path
                        |- orient_0
                            |-flair
                                |-t
                                |-nt
                                |-test
                                |-val
                                |-train_all
                            |-seg
                                ...
                            |-t1
                                ...
                            |-t2
                                ...
                            |-t1ce
                                ...
                        |-orient_1
                            ...
                        |-orient_2
                            ...
        orientation: 0, 1, 2 for 3 possible orientations
        data_split: 'train', 'val', 'test'
    """
    files=[]
    channels = ["seg","t1","t2","t1ce","flair"]
    print(channels)
    orient_sizes = [240, 240, 155]
    # find names of all the flair files and append to  a list
    # Gives us a list of all the cases
    for dirpath, dirname, filenames in os.walk(input_nii):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            valid = re.findall(r".*" + "flair.nii",fp)
            if len(valid) > 0:
                files.append(valid[0])

    for nii_file in files:
        folder = {}
        # set folder to split name, change later for train only
        for i in range(0, orient_sizes[orientation]):
            folder[i] = data_split
        print(re.sub("flair.nii"," ",nii_file))
        for channel in channels:
            # We only have seg for the trainig set
            if data_split != 'train' and channel == 'seg':
                continue
            img = nibabel.load(re.sub("flair.nii",channel+".nii",nii_file))
            arr_data = img.get_data()
            # For each seg file, change label to 3 if it is 4
            if channel == "seg":
                arr_data[arr_data == 4] = 3
                # Based on orientation, slice the images
                # Assign separate folders to slices with and without tumors
                for i in range(0, orient_sizes[orientation]):
                    if orientation == 0:
                        seg_slice = arr_data[i, :, :]
                    elif orientation == 1:
                        seg_slice = arr_data[:, i, :]
                    else:
                        seg_slice = arr_data[:, :, i]
                    if (seg_slice!=0).any():
                        folder[i] = "t"
                    else:
                        folder[i] = "nt"
            # Based on orientation, slice the images 
            # If required, apply padding       
            for i in range(0, orient_sizes[orientation]):
                if orientation == 0:
                    arr_slice_temp = arr_data[i, :, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42:arr_slice_temp.shape[1]+42] = arr_slice_temp
                elif orientation == 1:
                    arr_slice_temp = arr_data[:, i, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42 : arr_slice_temp.shape[1]+42] = arr_slice_temp
                else:
                    arr_slice = arr_data[:, :, i]

                img_slice=os.path.basename(re.sub("flair.nii",str(i)+"_"+channel+".png",nii_file))
                outFilepath = output_path + "/orient_" + str(orientation) + "/" + channel + "/" + folder[i] + "/" + img_slice
                # For seg images prevent rescaling but rescale other images
                if channel == "seg":
                    scipy.misc.toimage(arr_slice, cmin=0, cmax=255).save(outFilepath)
                else:
                    scipy.misc.toimage(arr_slice).save(outFilepath)
                # allows us to combine 3 consecutive slices easily later
                if data_split == 'train':
                    outFilepath = output_path + "/orient_" + str(orientation) + "/" + channel + "/" + 'train_all' + "/" + img_slice
                    scipy.misc.toimage(arr_slice).save(outFilepath)



def create_image_from_3slices(dir_path, input_files, output_path, channel, num_slices):
    """
    Function to combine 3 consecutive slices to rgb channels
    Args:
        dir_path: path to the directory containig slices
        input_files: Text file with list of patients
        output_path: Full path for storing created images
            Should be already created
        channel: flair, t1ce, t2, or t1
        num_slices: number of slices(depends only on orientation)
                possible values are 240 and 155
    """
    # create a list of all patient IDs
    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        print(patient)
        # combine 3 consecutive slices
        for i in range(0,num_slices):
            # border slices have one end missing, set to 0
            img_prev = numpy.zeros((240,240), dtype='int16')
            img_next = numpy.zeros((240,240), dtype='int16')
            this_img = dir_path + '/' +patient+'_'+str(i)+ '_' +channel+'.png'
            print(this_img)
            prev_img = dir_path + '/' + patient+'_'+str(i-1)+'_'+channel+'.png'
            next_img = dir_path + '/' + patient+'_'+str(i+1)+'_'+channel+'.png'
            img_this = scipy.misc.imread(this_img)
            # Do not try to read beyond the shape
            if i!=0:
                img_prev = scipy.misc.imread(prev_img)
            if i!=num_slices-1:
                img_next = scipy.misc.imread(next_img)

            # Combine 3 consecutive slices and save as rgb image
            img_ch3 = numpy.stack((img_prev,img_this,img_next), axis=2)
            img_slice=patient+'_'+str(i)+"_" +channel+"3.png"
            print(img_slice)
            outFilepath = output_path + "/" + img_slice
            scipy.misc.toimage(img_ch3).save(outFilepath)


def create_3channel_rgb(input_dir, output_path):
    """
    Function to combine flair, t1ce, and t2 slices
    Call separately for each orientation
    Args:
        input_dir: Path to directory of flair slices
        output_path: Path to dir for storing rgb slices
    """
    # Create a list of flair slice files
    files = []
    for dirpath, dirname,filenames in os.walk(input_dir):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        # Read corresponding flair, t1ce, t2 slices
        img_flair = scipy.misc.imread(re.sub("flair","flair",img_file))
        img_t1ce = scipy.misc.imread(re.sub("flair","t1ce",img_file))
        img_t2 = scipy.misc.imread(re.sub("flair","t2",img_file))
        # create rgb images with 3 channels of data
        img_rgb = numpy.stack((img_flair,img_t1ce,img_t2), axis=2)
        img_slice=os.path.basename(re.sub("flair","rgb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_rgb).save(outFilepath)


def create_3channel_t1ce_minus_t1(input_dir, output_path):
    """
    Function to combine flair, t1ce-t1, and t2 slices
    Call separately for each orientation
    Args:
        input_dir: Path to directory of flair slices
        output_path: Path to dir for storing rgb slices
    """

    resource.RLIMIT_NPROC=1
    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            print(fp)
            files.append(fp)


    for img_file in files:
        print(re.sub("flair","",img_file))
        # read input files
        img_flair = scipy.misc.imread(re.sub("flair","flair",img_file))
        img_t1ce = scipy.misc.imread(re.sub("flair","t1ce",img_file))
        img_t2 = scipy.misc.imread(re.sub("flair","t2",img_file))
        img_t1 = scipy.misc.imread(re.sub("flair","t1",img_file))
        # Compute t1-t1ce
        img_t1c = numpy.subtract(img_t11, img_t1ce1)
        img_slice=os.path.basename(re.sub("flair","t1c1",img_file))
        # create and save output image
        img_rdb = numpy.stack((img_flair,img_t2,img_t1c), axis=2)
        img_slice=os.path.basename(re.sub("flair","rdb",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_rdb).save(outFilepath)
