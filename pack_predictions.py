""" 
Code to create all packed inputs from predictions
    Args:
        output_path: Output location. Set up directory structure
                    output_path
                        |- pred0
                        |- pred1
                        |- pred2
                        |- dpred0
                        ...
                        |- flair3pred0
                        ...
                        |- t1ce3pred0
                        ...
                        |- t13pred0
                        ...
                        |- t23pred0
                        ...
                        |- wtrgb
                            |-pack0
                            |-pack1
                            |-pack2
                            |-prgb
                        |-etrgb
                            |-pack0
                            |-pack1
                            |-pack2
                            |-prgb

"""

from packing_utils import compute_majority 
from packing_utils import pack_pred_as_grayscale
from packing_utils import combine_packed_channels
import sys

if __name__=="__main__":
    output_path = sys.argv[1]
    compute_majority(output_path, '/path/to/input/files', 'flair3pred', output_path+'/flair3m', 0)
    compute_majority(output_path, '/path/to/input/files', 't23pred', output_path+'/t23m', 1)
    compute_majority(output_path, '/path/to/input/files', 't13pred', output_path+'/t13m', 2)
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/wtrgb/pack0', 0, ['flair3pred0','dpred0', 'pred0'])
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/wtrgb/pack1', 1, ['flair3pred1','dpred1', 'pred1'])
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/wtrgb/pack2', 2, ['flair3pred2','dpred2', 'pred2'])
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/etrgb/pack0', 0, ['flair3m', 't1ce3pred0','dpred0', 'pred0'])
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/etrgb/pack1', 1, ['t23m', 't1ce3pred1','dpred1', 'pred1'])
    pack_pred_as_grayscale(output_path, '/path/to/input/files', output_path+'/etrgb/pack2', 2, ['t13m', 't1ce3pred2','dpred2', 'pred2'])
    combine_packed_channels(output_path+'/wtrgb/pack0')
    combine_packed_channels(output_path+'/etrgb/pack0')
