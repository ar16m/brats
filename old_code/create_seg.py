import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_files, output_path):
    """
    Use this function to slice 3D images 0,1,2 orientations slice along different axes
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        img_seg = numpy.zeros((240,240), dtype='int16')
        img_slice=os.path.basename(re.sub("flair3","seg",img_file))
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_seg).save(outFilepath)
