
import nibabel
import scipy.misc
import numpy
import os
import re



def slicing3Dto2D(input_nii, output_path, orientation, data_split):
    """
    Use this function to slice 3D images 0,1,2 orientations slice
    along different axes
    Args:
        input_nii: Path to the top-level Brats data directory 
            (with HGG and LGG as subdirectories for train set;
            validation, test has each case under this folder)
        output_path: Output location. Set up directory structure
                    output_path
                        |- orient_0
                            |-flair
                                |-t
                                |-nt
                                |-test
                                |-val
                            |-seg
                                ...
                            |-t1
                                ...
                            |-t2
                                ...
                            |-t1ce
                                ...
                        |-orient_1
                            ...
                        |-orient_2
                            ...
        orientation: 0, 1, 2 for 3 possible orientations
        data_split: 'train', 'val', 'test'
    """
    files=[]
    channels = ["seg","t1","t2","t1ce","flair"]
    print(channels)
    orient_sizes = [240, 240, 155]
    # find names of all the flair files and append to  a list
    # Gives us a list of all the cases
    for dirpath, dirname, filenames in os.walk(input_nii):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            valid = re.findall(r".*" + "flair.nii",fp)
            if len(valid) > 0:
                files.append(valid[0])

    for nii_file in files:
        folder = {}
        # set folder to split name, change later for train only
        for i in range(0, orient_sizes[orientation]):
            folder[i] = data_split
        print(re.sub("flair.nii"," ",nii_file))
        for channel in channels:
            # We only have seg for the trainig set
            if data_split != 'train' and channel == 'seg':
                continue
            img = nibabel.load(re.sub("flair.nii",channel+".nii",nii_file))
            arr_data = img.get_data()
            # For each seg file, change label to 3 if it is 4
            if channel == "seg":
                arr_data[arr_data == 4] = 3
                # Based on orientation, slice the images
                # Assign separate folders to slices with and without tumors
                for i in range(0, orient_sizes[orientation]):
                    if orientation == 0:
                        seg_slice = arr_data[i, :, :]
                    elif orientation == 1:
                        seg_slice = arr_data[:, i, :]
                    else:
                        seg_slice = arr_data[:, :, i]
                    if (seg_slice!=0).any():
                        folder[i] = "t"
                    else:
                        folder[i] = "nt"
            # Based on orientation, slice the images 
            # If required, apply padding       
            for i in range(0, orient_sizes[orientation]):
                if orientation == 0:
                    arr_slice_temp = arr_data[i, :, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42:arr_slice_temp.shape[1]+42] = arr_slice_temp
                elif orientation == 1:
                    arr_slice_temp = arr_data[:, i, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42 : arr_slice_temp.shape[1]+42] = arr_slice_temp
                else:
                    arr_slice = arr_data[:, :, i]

                img_slice=os.path.basename(re.sub("flair.nii",str(i)+"_"+channel+".png",nii_file))
                outFilepath = output_path + "/orient_" + str(orientation) + "/" + channel + "/" + folder[i] + "/" + img_slice
                # For seg images prevent rescaling but rescale other images
                if channel == "seg":
                    scipy.misc.toimage(arr_slice, cmin=0, cmax=255).save(outFilepath)
                else:
                    scipy.misc.toimage(arr_slice).save(outFilepath)


