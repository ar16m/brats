"""
Code to create final predictions
    Args:
        output_path: Output location. Set up directory structure
                    output_path
                        |- wtpred (slices)
                        |- tcpred
                        |- etpred
                        |- wtnii (3D nii)
                        |- tcnii
                        |- etnii
                        |- comb (final nii pred)

"""

import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def recombine_preds_to3D(dir_path,input_files, output_path,orient):
    """
    Function to recombine the brain from slices 
    put orient=2 for final packed result
     Args:
        dir_path: Path to directory of final pred slices
        input_files: path to textfile with list of dir files
        output_path: Path to dir for storing nifti seg 3D
        orient: 0,1, or 2

    """
    # create a list of patient IDs
    resource.RLIMIT_NPROC=1
    f = open(input_files)
    lines = f.readlines()
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        print(patient)
        slices= []
        # stack to recreate volume
        for i in range(0,240):
            if orient==2 and (i < 42 or i > 196):
                temp = numpy.zeros((240,240), dtype='int16')
            elif orient==2:
                temp = scipy.misc.imread(dir_path+'/'+ patient+'_'+str(i-42)+'.png')
            else:
                temp = scipy.misc.imread(dir_path + '/' + patient +'_'+str(i)+'.png')
            slices.append(temp)
               

            volume = numpy.stack(slices, axis=orient)
         
        # remove padding by slicing axially and recombining
        predc_slices=[]
        for i in range(42,197):
            slice_pred = volume[:,:,i]
            predc_slices.append(slice_pred)

        vol_pred = numpy.stack(predc_slices, axis=2)
        vol_pred[vol_pred==3]=4
        print(numpy.count_nonzero(vol_pred))
        print(vol_pred.shape)
        pred_nii=nibabel.Nifti1Image(vol_pred, numpy.eye(4))
        nii_name=os.path.basename(patient)+".nii.gz"
        outFilepath = output_path + "/" + nii_name
        nibabel.save(pred_nii,outFilepath)


def combine_using_subset(wt_path, wt, tc, et, excl,output_path):
    """
    Use this function to combine the brain predictions
    for 3 classes
    Args:
        wt_path: path where wt seg 3D is stored
        wt: directory name of wt preds
        tc: directory name of tc preds
        et: directory name of et preds
        excl: whether to make exclusive comb.
            No tc outside wt, etc
        output_path: Path to final output
    """

    # create list of files
    files = []
    for dirpath, dirname,filenames in os.walk(wt_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 
    print(len(files))
    for ni_file in files:
        print(ni_file)
        # read 3 segmentation preds
        wt_img=nibabel.load(re.sub(wt,wt,ni_file)).get_data()
        tc_img=nibabel.load(re.sub(wt,tc,ni_file)).get_data()
        et_img=nibabel.load(re.sub(wt,et,ni_file)).get_data()
        wt_img[wt_img>0]=2
        # consider the subset relation
        if excl==1:
            tc_img[wt_img==0]=0
        tc_img[tc_img==4]=1
        wt_img[tc_img==1]=1
        if excl==1:
            et_img[wt_img==0]=0
            et_img[tc_img!=1]=0
        wt_img[et_img==4]=4
        # save final output
        img_pred=os.path.basename(ni_file)
        pred_nii=nibabel.Nifti1Image(wt_img, numpy.eye(4))
        outFilepath = output_path + "/" + img_pred
        nibabel.save(pred_nii,outFilepath)

if __name__=="__main__":
    output_path = sys.argv[1]
    recombine_preds_to3D(output_path+'/wtpred',input_files, output_path+'/wtnii', 2)
    recombine_preds_to3D(output_path+'/tcpred',input_files, output_path+'/tcnii', 2)
    recombine_preds_to3D(output_path+'/etpred',input_files, output_path+'/etnii', 2)
    combine_using_subset(output_path+'/wtnii', 'wtnii', 'tcnii', 'etnii', 1, output_path+'/comb'):
