import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_files, output_path):
    """
    Use this function to combine flair, t1ce, and t2 slices and pass path to flair as input_files
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 

    print(len(files))
    for img_file in files:
        img_seg = scipy.misc.imread(img_file)
        img_seg[img_seg>0]=1
        img_slice=os.path.basename(img_file)
        outFilepath = output_path + "/" + img_slice
        scipy.misc.toimage(img_seg).save(outFilepath)
