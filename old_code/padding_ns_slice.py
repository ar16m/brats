import nibabel
import scipy.misc
import numpy
import os
import re



def process(input_nii, output_path, orientation):
    """
    Use this function to slice 3D images 0,1,2 orientations slice
    along different axes
    input_nii is path to the top-level Brats data directory (this for validation)
    output_path is the output location and must have the directory structure set up. orientation is 0, 1, 2
    """
    files=[]
    channels = ["flair","t1","t2","t1ce"]
    print(channels)
    orient_sizes = [240, 240, 155]
    # find names of all the seg files and append to  a list
    for dirpath, dirname, filenames in os.walk(input_nii):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            valid = re.findall(r".*" + "flair.nii",fp)
            if len(valid) > 0:
                files.append(valid[0])

   # For each seg file, change label to 3 if it is 4
   # Assign separate folders to slices with and without tumors
    for nii_file in files:
        folder = {}
        print(re.sub("flair.nii"," ",nii_file))
        for channel in channels:
            img = nibabel.load(re.sub("flair.nii",channel+".nii",nii_file))
            arr_data = img.get_data()
    # Based on orientation, slice the images
     # If required, apply padding       
            for i in range(0, orient_sizes[orientation]):
                if orientation == 0:
                    arr_slice_temp = arr_data[i, :, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42:arr_slice_temp.shape[1]+42] = arr_slice_temp
                elif orientation == 1:
                    arr_slice_temp = arr_data[:, i, :]
                    arr_slice = numpy.zeros((240,240), dtype='int16')
                    arr_slice[0 : arr_slice_temp.shape[0]+0, 42 : arr_slice_temp.shape[1]+42] = arr_slice_temp
                else:
                    arr_slice = arr_data[:, :, i]
                img_slice=os.path.basename(re.sub("flair.nii",str(i)+"_"+channel+".png",nii_file))
                outFilepath = output_path + "/orient_" + str(orientation) + "/" + channel + "/" + img_slice
                # For seg images prevent rescaling but rescale other images
                scipy.misc.toimage(arr_slice).save(outFilepath)


