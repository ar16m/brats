# BraTS

The steps should be:
1. Create all the necessary directory structure for preprocessing_data.py
2. Execute preprocessing_data.py
3. Use deeplab to train and visualize results of 18 initial classifiers
4. Create necessary directory structure for pack_predictions.py
5. pack the raw annotations (predictions) using pack_predictions.py
6. Create necessary directories for postprocessing
7. Execute postprocessing.py to get final output 

