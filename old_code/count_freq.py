import nibabel
import scipy.misc
import numpy
import os
import re

def process(input_files):
    """
    Use this function to save the packed prediction slices as rgb slices
    """

    files = []
    for dirpath, dirname,filenames in os.walk(input_files):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 
    bg=0
    et=0
    ed=0
    ncr=0
    print("bg="+str(bg))
    print("et="+str(et))
    print("ed="+str(ed))
    print("ncr="+str(ncr))
    print(len(files))
    for img_file in files:
        img_seg = scipy.misc.imread(img_file)
        bg_temp=numpy.where(img_seg==0)[0].size
        et_temp=numpy.where(img_seg==1)[0].size
        ed_temp=numpy.where(img_seg==2)[0].size
        ncr_temp=numpy.where(img_seg==3)[0].size
        bg=bg+bg_temp
        et=et+et_temp
        ed=ed+ed_temp
        ncr=ncr+ncr_temp
 
    et_mult=bg/et
    ed_mult=bg/ed
    ncr_mult=bg/ncr
    
    print("bg_mult=1.00")
    print("et_mult="+str(et_mult))
    print("ed_mult="+str(ed_mult))
    print("ncr_mult="+str(ncr_mult))
    print("bg="+str(bg))
    print("et="+str(et))
    print("ed="+str(ed))
    print("ncr="+str(ncr))
