import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def pack_pred(dir_path, input_files, output_path, orient,pack_list):
    """
    Use this function to pack the predictions for different image combinations in one orientation (or after using majority voting for combining orientations with poor prediction
    """

    resource.RLIMIT_NPROC=1
    f = open(input_files)
    lines = f.readlines()
    
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        slices=[]
        print(patient)
        for i in range(0,len(pack_list)):
            slices.append([])
        for j in range(0, len(pack_list)):
            for i in range(0,240):
                if orient==2 and (i < 42 or i > 196):
                    temp = numpy.zeros((240,240), dtype='int16')
                elif orient==2:
                    temp = scipy.misc.imread(dir_path+ '/' + pack_list[j]+'/'+patient+'_'+str(i-42)+'.png')
                else:
                    temp = scipy.misc.imread(dir_path+ '/' + pack_list[j]+'/'+patient+'_'+str(i)+'.png')
                slices[j].append(temp)

        volumes=[]
        packed_vol=numpy.zeros((240,240,240), dtype='int16')
        for i in range(0,len(pack_list)):
            print(str(i))
            temp=numpy.stack(slices[i], axis=orient)
            volumes.append(temp)
            volumes[i]=volumes[i]*(2**(2*i))
            packed_vol=numpy.add(packed_vol,volumes[i])
       
        if len(pack_list) < 4:
            packed_vol=packed_vol*(4-len(pack_list))*4
            print(str(4-len(pack_list)))
        for i in range(42,197):
            slice_pred = packed_vol[:,:,i]
            img_slice=patient+'_'+str(i-42)+'_pack'+str(orient)+'.png'
            outFilepath = output_path + "/" + img_slice
            scipy.misc.toimage(slice_pred, cmin=0, cmax=255).save(outFilepath)


