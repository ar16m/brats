

NUM_STEPS=5000
while [ $NUM_STEPS -le 75000 ]
do
   python3 deeplab/train.py --logtostderr --training_number_of_steps=$NUM_STEPS --train_split="train" --model_variant="xception_65" --atrous_rates=6 --atrous_rates=12 --atrous_rates=18 --output_stride=16 --decoder_output_stride=4 --train_crop_size=241 --train_crop_size=241 --train_batch_size=8 --fine_tune_batch_norm=false --base_learning_rate=.005 --learning_rate_decay_step=30000 --dataset="brats" --tf_initial_checkpoint="/home/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/init_models/deeplabv3_pascal_train_aug/model.ckpt" --train_logdir="/home/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/exp/train_on_train_set/train/train_t1ce3_1" --dataset_dir="/home/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/tfrecord" > train_t1ce3_1/train_$NUM_STEPS 2>&1
#
#   echo $NUM_STEPS >> eval_2.txt
#   python3 deeplab/eval.py \
#       --logtostderr \
#       --eval_split="val" \
#       --model_variant="xception_65" \
#       --max_number_of_evaluations=1 \
#       --atrous_rates=6 \
#       --atrous_rates=12 \
#       --atrous_rates=18 \
#       --output_stride=16 \
#       --decoder_output_stride=4 \
#       --eval_crop_size=241 \
#       --eval_crop_size=241 \
#       --dataset="brats" \
#       --checkpoint_dir="/data/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/exp/train_on_train_set/train" \
#       --eval_logdir="/data/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/exp/train_on_train_set/eval" \
#       --dataset_dir="/data/ahana/tensorflow/tensorflow/models/research/deeplab/datasets/brats/tfrecord" >> train_voc_orient1_contrast/eval_1.txt 2>&1
   NUM_STEPS=`expr $NUM_STEPS + 5000`
done
