import nibabel
import scipy.misc
import numpy
import os
import re



def process(wt_path, wt, tc, et, excl,output_path):
    """
    Use this function to combine the brain predictions
    """

    files = []
    for dirpath, dirname,filenames in os.walk(wt_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            files.append(fp)
 
    print(len(files))
    for ni_file in files:
        print(ni_file)
        wt_img=nibabel.load(re.sub(wt,wt,ni_file)).get_data()
        tc_img=nibabel.load(re.sub(wt,tc,ni_file)).get_data()
        et_img=nibabel.load(re.sub(wt,et,ni_file)).get_data()
        wt_img[wt_img>0]=2
        if excl==1:
            tc_img[wt_img==0]=0
        tc_img[tc_img==4]=1
        wt_img[tc_img==1]=1
        if excl==1:
            et_img[wt_img==0]=0
            et_img[tc_img!=1]=0
        wt_img[et_img==4]=4
        img_pred=os.path.basename(ni_file)
        pred_nii=nibabel.Nifti1Image(wt_img, numpy.eye(4))
        outFilepath = output_path + "/" + img_pred
        nibabel.save(pred_nii,outFilepath)
