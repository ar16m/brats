import nibabel
import scipy.misc
import numpy
import os
import re
import resource



def process(dir_path,input_files, output_path,orient):
    """
    Use this function to recombine the brain from slices put orient=2 for final packed result
    """

    resource.RLIMIT_NPROC=1
    f = open(input_files)
    lines = f.readlines()
    #for dirpath, dirname,filenames in os.walk(input_files):
    #    for f in filenames:
    #        fp = os.path.join(dirpath, f)
#            print(fp)
    #        files.append(fp)
    st_lines=[]
    for line in lines:
        st_line=line.strip()
        st_lines.append(st_line)


    print(len(st_lines))
    for patient in st_lines:
        pred=[]
        print(patient)
        slices= []
        for i in range(0,240):
            if orient==2 and (i < 42 or i > 196):
                temp = numpy.zeros((240,240), dtype='int16')
            elif orient==2:
                temp = scipy.misc.imread(dir_path+'/'+ patient+'_'+str(i-42)+'.png')
            else:
                temp = scipy.misc.imread(dir_path + '/' + patient +'_'+str(i)+'.png')
            slices.append(temp)
               

            volume = numpy.stack(slices, axis=orient)
         

        predc_slices=[]
        for i in range(42,197):
            slice_pred = volume[:,:,i]
            predc_slices.append(slice_pred)


        vol_pred = numpy.stack(predc_slices, axis=2)
        vol_pred[vol_pred==1]=2
        print(numpy.count_nonzero(vol_pred))
        print(vol_pred.shape)
        pred_nii=nibabel.Nifti1Image(vol_pred, numpy.eye(4))
        nii_name=os.path.basename(patient)+".nii.gz"
        outFilepath = output_path + "/" + nii_name
        nibabel.save(pred_nii,outFilepath)
